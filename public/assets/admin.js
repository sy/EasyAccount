Vue.use(VueMaterial.default);

function Api(param) {
	return new Promise(resolve => {
		const fetchParam = {
			method: param.post ? 'POST' : 'GET',
			headers: {
				'X-Admin-Password': sessionStorage.getItem('easyaccount_admin')
			}
		};
		let url = `${BASE_URI}${param.url}`;
		if (param.query) {
			url += '?' + (new URLSearchParams(param.query)).toString();
		}
		if (fetchParam.method === 'POST') {
			fetchParam.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			const formBody = [];
			for (const property in param.post) {
				const encodedKey = encodeURIComponent(property);
				const encodedValue = encodeURIComponent(param.post[property]);
  				formBody.push(encodedKey + "=" + encodedValue);
			}
			fetchParam.body = formBody.join("&");
		}
		fetch(url, fetchParam)
		.then(r => r.json())
		.then(resolve);
	})
}

new Vue({
	el: '#app',
	data: function() {
		return {
			status: 0,
			activePage: "config",
			loginFrame: "",
			password: "",
			config: [],
			app: [],
			searchUserInput: "",
			toast: {
				show: false,
				message: ""
			},
			edit_user: {
				show: false,
				id: -1,
				nickname: "",
				realname: "",
				phone: "",
				mail: "",
				avatar: "",
				password: ""
			},
			resetPwd: {
				show: false,
				result: ""
			},
			setUserStatus: {
				show: false,
				id: -1,
				status: ""
			}
		};
	},
	methods: {
		showToast: function(msg) {
			this.toast.message = msg;
			this.toast.show = true;
		},
		switchPage: function(name) {
			this.activePage = name;
		},
		tryLogin: function() {
			const _this = this;
			_this.status = 0;
			Api({
				url: 'index/admin/login',
				post: {
					password: this.password
				}
			})
			.then(r => {
				if (r.success) {
					sessionStorage.setItem('easyaccount_admin', r.password);
					_this.status = 2;
					_this.showToast('登录成功');
				} else {
					_this.status = 1;
					_this.showToast(r.error);
				}
			});
		},
		loadConfig: function() {
			const _this = this;
			Api({
				url: 'admin/config/list'
			})
			.then(r => {
				if (r.success) {
					_this.config = r.list
				}
			});
		},
		saveConfig: function() {
			const data = [];
			const _this = this;
			_this.config.forEach(e => {
				data.push({
					id: e.id,
					value: e.value
				});
			});
			Api({
				url: 'admin/config/save',
				post: data
			})
			.then(r => {
				_this.showToast('已保存');
			});
		},
		loadApp: function() {
			const _this = this;
			Api({
				url: 'admin/app/list'
			})
			.then(r => {
				if (r.success) {
					_this.app = r.list;
				}
			});
		},
		showSecret: function(secret) {
			this.show_secret.content = secret;
			this.show_secret.show = true;
		},
		editUser: function(u) {
			if (u === null) {
				this.edit_user.id = -1;
				this.edit_user.nickname = "";
				this.edit_user.realname = "";
				this.edit_user.phone = "";
				this.edit_user.mail = "";
				this.edit_user.avatar = "";
			} else {
				this.edit_user.id = u.id;
				this.edit_user.nickname = u.nickname;
				this.edit_user.realname = u.realname;
				this.edit_user.phone = u.phone;
				this.edit_user.mail = u.mail;
				this.edit_user.avatar = u.avatar;
			}
			this.edit_user.password = "";
			this.edit_user.show = true;
		},
		editUserSave: function() {
			const _this = this;
			Api({
				url: 'admin/user/save',
				post: _this.edit_user
			})
			.then(r => {
				if (r.success) {
					_this.edit_user.show = false;
					_this.showToast('已保存');
				}
			})
		},
		delUser: function(u) {
			const _this = this;
			if (confirm(`确定要删除用户“${u.mail}”吗？此操作不可撤销`)) {
				Api({
					url: 'admin/user/del',
					post: {
						id: u.id
					}
				})
				.then(r => {
					if (r.success) {
						_this.showToast(`已删除用户“${u.mail}”`);
					}
				})
			}
		},
		showSetUserStatus: function(u) {
			this.setUserStatus.id = u.id;
			this.setUserStatus.status = u.status;
			this.setUserStatus.show = true;
		},
		onSetUserStatus: function() {
			const _this = this;
			Api({
				url: 'admin/user/status',
				post: {
					id: this.setUserStatus.id,
					status: this.setUserStatus.status
				}
			})
			.then(r => {
				if (r.success) {
					_this.showToast("已保存");
				}
			})
		},
		loginout: function() {
		}
	}
});