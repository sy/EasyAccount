/*
Navicat MySQL Data Transfer

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` varchar(20) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('cookie_prefix', 'ea_');
INSERT INTO `config` VALUES ('login_frame', '');
INSERT INTO `config` VALUES ('privapi_type', 'swoole');
INSERT INTO `config` VALUES ('privapi_key', '');
INSERT INTO `config` VALUES ('base_url', 'https://example.com/account/');
INSERT INTO `config` VALUES ('oauth_redirect', '');
INSERT INTO `config` VALUES ('oauth_secret', '');

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id` char(12) CHARACTER SET utf8mb4 NOT NULL,
  `verify` char(18) CHARACTER SET utf8mb4 NOT NULL,
  `uid` bigint(20) NOT NULL,
  `expire` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expire` (`expire`)
) ENGINE=InnoDB CHARACTER SET=utf8mb4;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `group_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `nickname` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `realname` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `phone` varchar(12) CHARACTER SET utf8mb4 NOT NULL,
  `mail` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT 'mail',
  `password` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `register` datetime NOT NULL,
  `avatar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail` (`mail`),
  KEY `gid` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET=utf8mb4;

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 CHARACTER SET=utf8mb4;

-- ----------------------------
-- Records of user_group
-- ----------------------------
INSERT INTO `user_group` VALUES ('1', '默认用户组');

--- ----------------------------
--- Table structure for user_oauth
--- ----------------------------
DROP TABLE IF EXISTS `user_oauth`;
CREATE TABLE `user_oauth`  (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(10) UNSIGNED NOT NULL,
  `appid` smallint(6) UNSIGNED NOT NULL,
  `nickname` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `openid` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `openid`(`openid`, `appid`),
  INDEX `uid`(`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET=utf8mb4;