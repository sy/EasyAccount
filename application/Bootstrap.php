<?php
use yesf\Yesf;
use yesf\library\Plugin;
use ea\library\PluginHandler;
use ea\model\Config;

class Bootstrap {
	public function run() {
		date_default_timezone_set('Asia/Shanghai');
		Yesf::getLoader()->addPsr4('ea\\library\\', APP_PATH . 'library');
		Plugin::register('workerStart', [PluginHandler::class, 'onWorkerStart']);
		Plugin::register('routerStart', [PluginHandler::class, 'onRouterStart']);
		Plugin::register('beforeDispatcher', [PluginHandler::class, 'onBeforeDispatcher']);
		Config::getInstance();
	}
}