<?php
return [
	'name' => 'EasyAccount',
	'namespace' => 'ea',
	'charset' => 'utf-8',
	'bootstrap' => 'Bootstrap',
	'router' => [
		'type' => 'utf-8',
		'extension' => FALSE
	],
	'modules' => ['admin', 'index'],
	'module' => 'index',
	'view' => [
		'auto' => FALSE,
		'extension' => 'phtml'
	],
	'tcp' => [
		'ip' => '0.0.0.0',
		'port' => 9502
	]
];