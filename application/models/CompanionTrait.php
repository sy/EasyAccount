<?php
/**
 * 伴生对象兼容模块
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Model
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\model;

trait CompanionTrait {
	//获取伴生对象
	public static function getCompanion() {
		static $_instance = NULL;
		if ($_instance === NULL) {
			$self_name_index = strrpos(__CLASS__, '\\');
			$class_name = substr(__CLASS__, 0, $self_name_index) . '\\Companion' . substr(__CLASS__, $self_name_index + 1);
			$_instance = $class_name::getInstance();
		}
		return $_instance;
	}
	//伴生对象调用
	public static function __callStatic($name, $arguments) {
		$clazz = self::getCompanion();
		if (method_exists($clazz, $name)) {
			return $clazz->$name(...$arguments);
		}
	}
}