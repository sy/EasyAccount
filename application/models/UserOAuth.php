<?php
/**
 * UserOAuth模块
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Model
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\model;

use yesf\library\ModelAbstract;
use ea\library\Cache;

class UserOAuth {
	protected static $_table_name = 'user_oauth';
	protected static $_primary_key = '';
}