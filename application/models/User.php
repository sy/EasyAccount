<?php
/**
 * 用户模块
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Model
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\model;

use yesf\library\ModelAbstract;

class CompanionUser extends ModelAbstract {
	protected static $_table_name = 'user';
	protected static $_primary_key = 'id';
}
class User {
	use CompanionTrait;
	/**
	 * 新增一个用户
	 * @access public
	 * @param array $user 用户信息
	 * @return string
	 */
	public static function add($user) {
		$user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
		if (!isset($user['register'])) {
			$user['register'] = date('Y-m-d H:i:s');
		}
		return self::getCompanion()->add($user);
	}
	/**
	 * 更新用户
	 * @access public
	 * @param array $user 用户信息
	 * @param int $id 用户ID
	 * @return string
	 */
	public static function set(array $user, int $id) {
		if (isset($user['password'])) {
			$user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
		}
		self::getCompanion()->set($user, $id);
		return TRUE;
	}
	/**
	 * 获取多个用户
	 * @access public
	 * @param array $filter 搜索条件
	 * @return array
	 */
	public static function list($filter = [], $num = 30, $offset = 0) {
		return self::getCompanion()->list($filter, $num, $offset, ['id', 'role', 'group_id', 'status', 'nickname', 'realname', 'phone', 'mail', 'register', 'avatar']);
	}
	/**
	 * 验证一个用户名/密码
	 * @access public
	 * @param string $type
	 * @param string $user
	 * @param string $password
	 * @return boolean/array
	 */
	public static function verify($type, $user, $password) {
		if (!is_string($user) || !is_string($password)) {
			return FALSE;
		}
		$user = self::getCompanion()->get([
			$type => $user
		]);
		if (password_verify($password, $user['password'])) {
			return $user;
		} else {
			return FALSE;
		}
	}
}