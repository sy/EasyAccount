<?php
/**
 * 用户组模块
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Model
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\model;

use yesf\library\ModelAbstract;

class CompanionUserGroup extends ModelAbstract {
	protected static $_table_name = 'user_group';
	protected static $_primary_key = 'id';
}
class UserGroup {
	use CompanionTrait;
	/**
	 * 新增一个用户组
	 * 
	 * @access public
	 * @param string $name 用户组名称
	 * @return int
	 */
	public static function add($name) {
		return self::getCompanion()->add([
			'name' => $name
		]);
	}
	/**
	 * 修改用户组
	 * 
	 * @access public
	 * @param int $id ID
	 * @param string $name 用户组名称
	 * @return int
	 */
	public static function set($id, $name) {
		return self::getCompanion()->set([
			'name' => $name
		], $id);
	}
}