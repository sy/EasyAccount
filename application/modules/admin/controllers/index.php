<?php
/**
 * 后台不需要检查登录状态的部分页面
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */

namespace ea\controller\admin;
use \yesf\Yesf;
use \yesf\library\ControllerAbstract;
use \ea\model\User as UserModel;

class Index extends ControllerAbstract {
	public static function indexAction($request, $response) {
		$response->header('Content-Type', 'text/html; charset=UTF-8');
		$response->display('index/index');
	}
	public static function loginAction($request, $response) {
		$response->header('Content-Type', 'application/json; charset=UTF-8');
	}
}