<?php
/**
 * 用户管理
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\controller\admin;

use yesf\Yesf;
use yesf\library\ControllerAbstract;
use ea\model\User;
use ea\model\UserGroup;
use ea\library\Utils;

class Group extends ControllerAbstract {
	/**
	 * 获取用户组列表
	 * 
	 * @api {get} /admin/group/list 获取配置列表
	 * @apiName GetGroupList
	 * @apiGroup Admin
	 * 
	 * @apiSuccess {Object[]} list 列表
	 * @apiSuccess {String} list.id ID
	 * @apiSuccess {String} list.name 名称
	 */
	public static function listAction($request, $response) {
		$list = UserGroup::list();
		$response->write(Utils::getWebApiResult(['list' => $list]));
	}
	/**
	 * 保存用户组
	 * 
	 * @api {post} /admin/group/save 保存用户组
	 * @apiName SaveGroup
	 * @apiGroup Admin
	 * 
	 * @apiParam {Int} id ID
	 * @apiParam {String} name 名称
	 * @apiSuccess {Int} id ID
	 */
	public static function saveAction($request, $response) {
		$id = intval($request->post['id']);
		$name = $request->post['name'];
		if ($id === -1) {
			$id = UserGroup::add($name);
		} else {
			UserGroup::set($id, $name);
		}
		$response->write(Utils::getWebApiResult(['id' => $id]));
	}
}
