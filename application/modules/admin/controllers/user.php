<?php
/**
 * 用户管理
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\controller\admin;

use yesf\Yesf;
use yesf\library\ControllerAbstract;
use ea\model\User as UserModel;
use ea\model\UserGroup;
use ea\model\Token;
use ea\library\Utils;

class User extends ControllerAbstract {
	/**
	 * 获取用户信息
	 * 
	 * @api {get} /admin/user/get 获取用户信息
	 * @apiName GetUser
	 * @apiGroup Admin
	 * 
	 * @apiParam {Int} id ID
	 * @apiSuccess {Object} user 用户
	 * @apiSuccess {Int} user.id ID
	 * @apiSuccess {Int} user.role 角色（管理员/非管理员）
	 * @apiSuccess {Int} user.status 状态
	 * @apiSuccess {Int} user.group_id 用户组ID
	 * @apiSuccess {String} user.nickname 昵称
	 * @apiSuccess {String} user.realname 姓名
	 * @apiSuccess {String} user.phone 手机号
	 * @apiSuccess {String} user.mail 邮箱
	 * @apiSuccess {String} user.register 注册时间
	 * @apiSuccess {String} user.avatar 头像地址
	 */
	public static function getAction($request, $response) {
		$id = intval($request->get['id']);
		$u = UserModel::get($id);
		unset($u['password']);
		$response->write(Utils::getWebApiResult(['user' => $u]));
	}
	/**
	 * 获取用户列表
	 * 
	 * @api {get} /admin/user/list 获取用户列表
	 * @apiName GetUserList
	 * @apiGroup Admin
	 * 
	 * @apiParam {Int} page ID
	 * @apiSuccess {Object[]} list 列表，字段见“获取用户”接口
	 */
	public static function listAction($request, $response) {
		$page = isset($request->get['page']) ? intval($request->get['page']) : 1;
		$skip = ($page - 1) * 30;
		$list = UserModel::list([], 30, $skip);
		$response->write(Utils::getWebApiResult(['list' => $list]));
	}
	/**
	 * 设置用户状态
	 * 
	 * @api {post} /admin/user/status 设置用户状态
	 * @apiName SetUserStatus
	 * @apiGroup Admin
	 * 
	 * @apiParam {Int} id ID
	 * @apiParam {Int} status status
	 */
	public static function statusAction($request, $response) {
		$id = intval($request->get['id']);
		//创始人账户不可操作
		$user = UserModel::get($id);
		$status = intval($request->get['status']);
		$rs = TRUE;
		if ($user['status'] != $status) {
			$rs = UserModel::set(['status' => $status], $id);
		}
		//失效所有token
		Token::delUser($id);
		if ($rs === FALSE) {
			$response->write(Utils::getWebApiResult(['error' => '操作失败']));
		} else {
			$response->write(Utils::getWebApiResult());
		}
	}
	/**
	 * 保存用户
	 * 
	 * @api {post} /admin/user/save 保存用户
	 * @apiName SaveUser
	 * @apiGroup Admin
	 * 
	 * @apiParam {Int} id ID
	 * @apiParam {String} name 名称
	 * @apiSuccess {Int} id ID
	 */
	public static function saveAction($request, $response) {
		$role = intval($request->post['role']);
		$id = intval($request->post['id']);
		if ($role !== 1 && $role !== 2) {
			$response->write(Utils::getWebApiResult(['error' => '用户类型无效']));
			return;
		}
		if ($id !== -1) {
			$user = UserModel::get($id);
			//普通管理员无权修改其他管理员的用户类型
			if ($request->user !== 'super' && ($request->user['role'] == 2 && $user['role'] == 2)) {
				$response->write(Utils::getWebApiResult(['error' => '您无权修改管理员的用户类型']));
				return;
			}
		}
		//普通管理员无权设置其他管理员
		if ($request->user !== 'super' && $role === 2) {
			$response->write(Utils::getWebApiResult(['error' => '您无权设置新管理员']));
			return;
		}
		$data = [
			'role' => $role,
			'group_id' => intval($request->post['group']),
			'nickname' => $request->post['nickname'],
			'realname' => $request->post['realname'],
			'phone' => $request->post['phone'],
			'mail' => $request->post['mail'],
			'avatar' => $request->post['avatar']
		];
		//如果是新增，则传递密码
		if ($id === -1) {
			$data['password'] = empty($request->post['password']) ? $request->post['password'] : Utils::randStr(8);
			$id = UserModel::add($data);
		} else {
			if (!empty($request->post['password'])) {
				$data['password'] = $request->post['password'];
				//失效所有token
				Token::delUser($id);
			}
			UserModel::set($data, $id);
		}
		$response->write(Utils::getWebApiResult([
			'id' => $id,
			'password' => $data['password']
		]));
	}
	/**
	 * 删除用户
	 * 
	 * @api {post} /admin/user/del 删除用户
	 * @apiName DelUser
	 * @apiGroup Admin
	 * 
	 * @apiParam {Int} id ID
	 */
	public static function delAction($request, $response) {
		$id = intval($request->get['id']);
		//创始人账户不可操作
		$user = UserModel::get($id);
		if ($user['role'] == 2) {
			$response->write(Utils::getWebApiResult(['error' => '创始人账户不可删除']));
			return;
		}
		$rs = UserModel::del($id);
		Token::delUser($id);
		if ($rs === FALSE) {
			$response->write(Utils::getWebApiResult(['error' => '操作失败']));
		} else {
			$response->write(Utils::getWebApiResult());
		}
	}
}
