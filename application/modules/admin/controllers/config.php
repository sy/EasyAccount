<?php
/**
 * 设置
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link http://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\controller\admin;

use yesf\Yesf;
use yesf\library\ControllerAbstract;
use ea\model\Config as ConfigModel;
use ea\model\OAuthClient;
use ea\library\Utils;

class Config extends ControllerAbstract {
	const CONFIG_LIST = [
		['cookie_prefix', 'Cookie认证前缀'],
		['login_frame', '普通登录地址'],
		['privapi_type', '私有API序列化方式（serialize/swoole/json）'],
		['privapi_key', '私有API密钥'],
		['base_url', '基础URL'],
		['default_redirect', '默认登录回调地址'],
		['oauth_redirect', 'OAuth回调地址'],
		['oauth_secret', 'OAuth加密密钥']
	];
	/**
	 * 获取配置列表
	 * 
	 * @api {get} /admin/config/list 获取配置列表
	 * @apiName GetConfigList
	 * @apiGroup Admin
	 * 
	 * @apiSuccess {Object[]} list 配置列表
	 * @apiSuccess {String} list.id 配置项ID
	 * @apiSuccess {String} list.name 配置项名称
	 * @apiSuccess {String} list.value 配置项内容
	 */
	public static function listAction($request, $response) {
		$list = [];
		foreach (self::CONFIG_LIST as $v) {
			$list[] = [
				'id' => $v[0],
				'name' => $v[1],
				'value' => ConfigModel::getInstance()->read($v[0])
			];
		}
		$response->write(Utils::getWebApiResult([
			'list' => $list
		]));
	}
	/**
	 * 保存配置
	 * 
	 * @api {get} /admin/config/save 保存配置
	 * @apiName SaveConfig
	 * @apiGroup Admin
	 * 
	 */
	public static function saveAction($request, $response) {
		foreach ($request->post as $key => $value) {
			ConfigModel::getInstance()->save($key, $value);
		}
		$response->write(Utils::getWebApiResult());
	}
}