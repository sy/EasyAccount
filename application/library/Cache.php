<?php
/**
 * Redis缓存类
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Library
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2018 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library;

use yesf\library\database\Database;

class Cache {
	const PREFIX = 'ea_';
	public static function get($k) {
		$result = Database::get('redis')->get(self::PREFIX . $k);
		return $result ? swoole_unserialize($result) : NULL;
	}
	public static function exists($k) {
		$result = Database::get('redis')->exists(self::PREFIX . $k);
		return $result ? TRUE : FALSE;
	}
	public static function del($k) {
		return Database::get('redis')->delete(self::PREFIX . $k);
	}
	public static function set($k, $v, $ttl = 7200) {
		Database::get('redis')->set(self::PREFIX . $k, swoole_serialize($v), $ttl);
	}
	public static function ttl($k, $ttl = 7200) {
		Database::get('redis')->expire(self::PREFIX . $k, $ttl);
	}
}