<?php
/**
 * OAuth接口
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Library
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library\OAuthClient;

interface OAuthInterface {
	/**
	  * 设置基本选项
	  * @param array $config
	  */
	public static function set($config);

	/**
	 * 获取登录链接
	 * @param string $redirect_uri 返回地址
	 * @return string
	 */
	public static function getAuthorizeURL($redirect_uri);

	/**
	 * 由AuthorizeCode换取AccessToken
	 * @param string $code AuthorizeCode
	 * @param string $redirect_uri 返回地址
	 * @return array
	 */
	public static function getAccessToken($code, $redirect_uri);

	/**
	 * 获取用户的基本信息
	 * @param string $token AccesToken
	 * @return array
	 */
	public static function getMyInfo($token);

}