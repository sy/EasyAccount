<?php
/**
 * 百度OAuth支持类
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Library
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library\OAuthClient;
use \Swoole\Coroutine as co;
use \Swoole\Coroutine\Http\Client;

class Baidu implements OAuthInterface {
	const ID = 1;

	const API_DOMAIN = 'openapi.baidu.com';
	const API_PORT = 443;
	const API_SSL = TRUE;
	const API_URL = 'https://openapi.baidu.com/';

	protected static $config = NULL;

	public static function set($config) {
		self::$config = $config;
	}

	public static function getAuthorizeURL($redirect_uri) {
		return self::API_URL . 'oauth/2.0/authorize?' . http_build_query([
			'client_id' => self::$config['client_id'],
			'response_type' => 'code',
			'redirect_uri' => $redirect_uri
		]);
	}

	public static function getAccessToken($code, $redirect_uri) {
		$data = [
			'grant_type' => 'authorization_code',
			'code' => $code,
			'client_id' => self::$config['client_id'],
			'client_secret' => self::$config['client_secret'],
			'redirect_uri' => $redirect_uri
		];
		$ip = co::gethostbyname(self::API_DOMAIN);
		$client = new Client($ip, self::API_PORT, self::API_SSL);
		$client->setHeaders([
			'Host' => self::API_DOMAIN
		]);
		$client->post('/oauth/2.0/token', $data);
		$result = json_decode($client->body, 1);
		return [
			'access_token' => $result['access_token']
		];
	}

	public static function getMyInfo($token) {
		$data = 'access_token=' . urlencode($token);
		$ip = co::gethostbyname(self::API_DOMAIN);
		$client = new Client($ip, self::API_PORT, self::API_SSL);
		$client->setHeaders([
			'Host' => self::API_DOMAIN
		]);
		$client->get('/rest/2.0/passport/users/getInfo?' . $data);
		$result = json_decode($client->body, 1);
		return [
			'id' => $result['userid'],
			'name' => $result['username'],
			'avatar' => 'http://tb.himg.baidu.com/sys/portrait/item/' . $result['portrait']
		];
	}
}