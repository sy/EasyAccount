<?php
/**
 * User API
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library\APIHandler;

use ea\model\User;
use ea\model\Token;
use ea\model\UserGroup;
use ea\model\OAuthClient;

class Group {
	//获取用户组列表
	public static function lists() {
		$list = UserGroup::list();
		return ['lists' => $list];
	}
	//添加用户组
	public static function add($name) {
		$id = UserGroup::add($name);
		return ['id' => $id];
	}
	//查找用户
	public static function user($id, $num = 20, $offset = 0) {
		return ['lists' => User::list([
				'group_id' => $id
			], $num, $offset)
		];
	}
}