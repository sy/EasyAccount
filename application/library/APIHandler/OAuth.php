<?php
/**
 * User API
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library\APIHandler;

use ea\model\User;
use ea\model\Token;
use ea\model\UserGroup;
use ea\model\OAuthClient;

class OAuth extends ControllerAbstract {
	//OAuth，绑定
	public static function add($data) {
		$uid = $data['uid'];
		if ($data['info']) {
			//解密
			$data = OAuthClient::encrypt($data['info'], TRUE);
		}
		UserOAuth::add([
			'uid' => $uid,
			'appid' => $data['appid'],
			'openid' => $data['id']
		]);
		return [];
	}
	public static function del($data) {
		if ($data['id']) {
			UserOAuth::del([
				'id' => $data['id']
			]);
		}
		UserOAuth::del([
			'uid' => $data['uid'],
			'appid' => $data['appid']
		]);
		return [];
	}
	public static function get($id) {
		$list = UserOAuth::list([
			'uid' => $id
		]);
		foreach ($list as &$v) {
			$v['appname'] = OAuthClient::get($v['appid'])['name'];
		}
		return ['lists' => $list];
	}
}