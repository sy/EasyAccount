<?php
/**
 * User API
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library\APIHandler;

use ea\model\User;
use ea\model\Token as TokenModel;
use ea\model\User as UserModel;
use ea\model\UserGroup;
use ea\model\OAuthClient;

class Token {
	public static function get($token) {
		//检查token是否有效
		$token = TokenModel::get($token);
		if (!$token) {
			return ['valid' => FALSE];
		} else {
			$token['valid'] = TRUE;
			//将用户信息组合进来
			$user = UserModel::get($token['uid']);
			unset($user['password']);
			$token['user'] = $user;
			return $token;
		}
	}
	//创建token
	public static function add($id) {
		list($token, $verify) = TokenModel::add($id);
		return ['token' => $token, 'verify' => $verify];
	}
	public static function del($token) {
		TokenModel::del($token);
		return [];
	}
}