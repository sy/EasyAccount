<?php
/**
 * API
 * 
 * @author ShuangYa
 * @package EasyAccount
 * @category Library
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2017 ShuangYa
 * @license https://www.sylibs.com/go/easyaccount/license
 */
namespace ea\library;

use ea\library\APIHandler\User;
use ea\library\APIHandler\Token;
use ea\library\APIHandler\Group;
use ea\library\APIHandler\OAuth;
use ea\model\OAuthClient;

class Api {
	public static function dispatch($action, $data) {
		switch ($action) {
			case 'user.get':
				return User::get($data['id']);
			case 'user.add':
				$data['id'] = -1;
				return User::save($data);
			case 'user.set':
				return User::save($data);
			case 'user.status':
				return User::status($data['id'], $data['status']);
			case 'user.del':
				return User::del($data['id']);
			case 'user.verify':
				return User::verify($data['type'], $data['user'], $data['password']);
			case 'token.get':
				return Token::get($data['token']);
			case 'token.add':
				return Token::add($data['id']);
			case 'token.del':
				return Token::del($data['token']);
			case 'group.add':
				return Group::add($data['name']);
			case 'group.lists':
				return Group::lists();
			case 'group.user':
				return Group::user($data['id'], $data['amount'], $data['offset']);
			case 'group.move':
				return User::set(['id' => $data['uid'], 'group_id' => $data['id']]);
			case 'oauth.get':
				return OAuth::get($data['id']);
			case 'oauth.add':
				return OAuth::add($data);
			case 'oauth.del':
				return OAuth::del($data);
			case 'oauth.lists':
				return OAuthClient::lists();
			default:
				return 'unsupported action';
		}
	}
}